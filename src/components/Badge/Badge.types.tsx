type IBadgeProp = {
    size : 'sm' | 'md' | 'lg'
    label : string , 
    svgPosition? :  'left' | 'right' , 
    type? : 'success' | 'danger' | 'normal' | 'warning'  , 
    className? : string
    svg? : string ,
}
export default IBadgeProp