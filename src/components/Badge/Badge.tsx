import React from "react";
import IBadgeProp from "./Badge.types";
import "./styles/Badge.scss";

export const Badge = ({
  label,
  size = 'md',
  type = "success",
  svg,
  svgPosition,
}: IBadgeProp) => {
  const svgDiv = (path) => {
    return (
      <div className="svg">
        <img src={path} />
      </div>
    );
  };
  let LIcon, RIcon;

  if (svgPosition === "left") {
    LIcon = svgDiv(svg);
  }
  if (svgPosition === "right") {
    RIcon = svgDiv(svg);
  }

  return (
    <div className={`badge-rnr badge-${type}`}>
      {}
      <div className={`badge-label  badge-${size} `}>
        {(svgPosition)? LIcon:RIcon}
        {svgPosition === 'left' && <></>}
        {label}
        
        {RIcon}
      </div>
    </div>
  );
};
