import React from "react";
import { Badge } from "./Badge";
import rightArrow from "../../assets/ico/arrow-right.svg";

class IndexBadge extends React.PureComponent {
  render() {
    return (
      <div>
        <div className="wrapper">
          <Badge
            svg={rightArrow}
            label="Badge"
            size="sm"
            type="success"
            svgPosition="left"
          />
          <Badge
            label="Badge"
            size="md"
            type="success"
            svg={rightArrow}
            svgPosition="right"
          />
          <Badge label="Badge" size="lg" type="success" />
        </div>

        <div className="wrapper">
          <Badge label="Badge" size="sm" type="warning" />
          <Badge label="Badge" size="md" type="warning" />
          <Badge label="Badge" size="lg" type="warning" />
        </div>
        <div className="wrapper">
          <Badge label="Badge" size="sm" type="danger" />
          <Badge label="Badge" size="md" type="danger" />
          <Badge label="Badge" size="lg" type="danger" />
        </div>

        <div className="wrapper">
          <Badge label="Badge" size="sm" type="normal" />
          <Badge label="Badge" size="md" type="normal" />
          <Badge label="Badge" size="lg" type="normal" />
        </div>
      </div>
    );
  }
}


export default IndexBadge