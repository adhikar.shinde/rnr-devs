import React from "react";
import { Toggle } from "./Toggle";

class IndexToggle extends React.PureComponent {
  render() {
    let title = "Remember Me";
    let subTitle = "Save my login details for next time.";
    return (
      <div>
        <Toggle title={title} subTitle={subTitle}  />
        <Toggle title={title} subTitle={subTitle} isDark={true} />
        <Toggle title={title} subTitle={subTitle} isBorder={true}/>
        <Toggle title={title} subTitle={subTitle} isDisabled={true} />
      </div>
    );
  }
}

export default IndexToggle;
