import React from "react";
import "./styles/style.scss";
import IBadgeProp from "./Toggle.types";

export const Toggle = ( {
    isDark = false ,  
    isBorder = false , 
    isDisabled = false , 
    isRemembered = false , 
    title , 
    subTitle , 
} ) => {
  return (
    <div className="content" >
      <div className="input-div" >
        <label className={"switch " + ( isDark ? 'is-dard' : '' ) }  >
          <input type="checkbox" />
          <span className={"slider round " + ( isBorder ? 'is-border ' : '')  }    ></span>
        </label>
      </div>
      <div className="text-div" >
          <div className="title" > 
              {title}
              
               </div>
               <div  className="subtitle" > {subTitle} </div>
          
           </div>
    </div>
  );
};
