
type IBadgeProp = {
    isDark? : true | false , 
    isBorder? : true | false , 
    isDisabled? : true | false , 
    isRemembered? : true | false , 
    title : string , 
    subTitle : string 
}

export default IBadgeProp

