import { MouseEventHandler } from "react";

type ButtonProps = {
    id?: string,
    className?: string,
    type?: "primary" | "secondary" | "info" | "light" | "danger",
    size?: "xl" | "lg" | "md" | "sm" | "xs",
    label: string,
    handleClick?: MouseEventHandler<HTMLButtonElement> | undefined;
}

export default ButtonProps;