import React from "react";
import './styles/Button.scss';
import IButtonProps  from "./Button.types";

export const Button = ({ id, className, type = "primary", size = "lg", label, handleClick }: IButtonProps) => {
    return (
        <button
            id={id}
            type="button"
            className={`btn-rnr ${className} btn-type-${type} btn-size-${size}`}
            onClick={handleClick}>
                {label}
        </button>
    )
}
