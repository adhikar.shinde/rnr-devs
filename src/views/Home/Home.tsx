import React from "react";

import "./styles/Home.scss";

import { Button } from "components/Button/Button";
import IndexBadge from "components/Badge/IndexBadge";
import IndexToggle from "components/Toggle/IndexToggle";

class Home extends React.PureComponent {
  render() {
    return (
      <div>
        {/* <Button className="sample-btn" type="danger" size="sm" label="Sample" /> */}

        {/* <IndexBadge /> */}

        <IndexToggle />
      </div>
    );
  }
}

export default Home;
