import * as webpack from 'webpack';
import * as webpackDevServer from 'webpack-dev-server';
import path from "path";
import { Configuration, DefinePlugin } from "webpack";
import HtmlWebpackPlugin from "html-webpack-plugin";
import ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin";
import TsconfigPathsPlugin from "tsconfig-paths-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
const webpackConfig = (): webpack.Configuration => ({
    entry: [
        './src/index.tsx',
        './src/styles/base.scss',
    ],
    resolve: {
        extensions: [".ts", ".tsx", ".js"],
        plugins: [new TsconfigPathsPlugin({ configFile: "./tsconfig.json" })],
        mainFields: ["module", "browser", "main"],
        alias: {
            "@styles": path.resolve(__dirname, "./src/styles"),
            "@views": path.resolve(__dirname, "./src/views"),
        }
    },
    output: {
        path: path.join(__dirname, "/dist"),
        filename: "bundle.js",
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "ts-loader",
                options: {
                    transpileOnly: true,
                },
                exclude: /dist/,
            },
            {
                test: /\.s?css$/,
                use: ["style-loader", "css-loader", "sass-loader"],
            },
            {
                test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
                type: 'asset/inline',
            },
        ],
    },
    devServer: {
        port: 3000,
        open: true,
        historyApiFallback: true,
    },
    plugins: [
        // HtmlWebpackPlugin simplifies creation of HTML files to serve your webpack bundles
        new HtmlWebpackPlugin({
            template: "./public/index.html",
            favicon: "./src/assets/images/favicon.ico"
        }),
        // DefinePlugin allows you to create global constants which can be configured at compile time
        new DefinePlugin({
            "process.env": process.env.production || !process.env.development,
        }),
        // MiniCssExtractPlugin extracts CSS into separate files to support On-Demand-Loading of CSS and SourceMaps.
        new MiniCssExtractPlugin({
            filename: "[hash].css",
        }),
        // Speeds up TypeScript type checking and ESLint linting (by moving each to a separate process)
        new ForkTsCheckerWebpackPlugin(),
    ],
});
export default webpackConfig;